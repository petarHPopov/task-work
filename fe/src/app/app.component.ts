import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppService} from './app.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {

  public title = 'client';
  public isSuccess = true;

  public myForm: FormGroup;

  public users = [];


  constructor(private service: AppService) {

    this.myForm = new FormGroup({
      firstName: new FormControl('', Validators.minLength(5)),
      lastName: new FormControl('', Validators.minLength(5)),
      email: new FormControl(''),
    }, {updateOn: 'blur'});

  }

  public ngOnInit(): void {
    this.myForm.valueChanges.subscribe((data) => {
      this.isSuccess = true;
    });
  }

  public ngOnDestroy(): void {
  }

  public createUser(): void {
    // this.service.postUser(this.myForm.value).subscribe((data) => {
    //
    //   if (data && data.status === 200) {
    //     this.users = [...this.users, this.myForm.value];
    //   } else {
    //     this.isSuccess = false;
    //   }
    //   this.myForm.reset();
    // });

    this.service.postUser(this.myForm.value).subscribe({
      next: () => {
        this.users = [...this.users, this.myForm.value];
      },
      // tslint:disable-next-line:object-literal-sort-keys
      error: (err: any) => this.isSuccess = false,
    });

  }
}

package com.work.job.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.work.job.model.User;
import com.work.job.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Override
    public ResponseEntity<?> checkUser(User user) {
        if(!checkUserValid(user)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private boolean checkUserValid(User user){
        return user.getFirstName().length() >= 5 && user.getLastName().length() >= 5;
    }
}

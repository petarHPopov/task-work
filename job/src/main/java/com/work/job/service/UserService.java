package com.work.job.service;

import org.springframework.http.ResponseEntity;

import com.work.job.model.User;

public interface UserService {

    ResponseEntity<?> checkUser(User user);

}

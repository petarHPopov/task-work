package com.work.job.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.work.job.model.User;
import com.work.job.service.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200/", maxAge = 3600)
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping()
    public ResponseEntity<?> users(@RequestBody User user){
           return userService.checkUser(user);
    }
}

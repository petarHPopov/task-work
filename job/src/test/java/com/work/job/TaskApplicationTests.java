package com.work.job;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.work.job.model.User;

@SpringBootTest
class TaskApplicationTests extends TestBase {

    @Test()
    @DisplayName("Check first name with more than 5 characters")
    void shouldReturnStatusOKWithFirstName() {
        user.setFirstName("Test1");
        user.setLastName("Lastname");
		ResponseEntity response = userService.checkUser(user);
		assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    @DisplayName("Check last name with more than 5 characters")
    void shouldReturnStatusOKWithLastName() {
        user.setFirstName("Test1");
        user.setLastName("Lastname");
        ResponseEntity response = userService.checkUser(user);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    @DisplayName("Check first name with less than 5 characters")
    void shouldReturnStatusBadRequestWithFirstname() {
        user.setFirstName("a");
        ResponseEntity response = userService.checkUser(user);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    @DisplayName("Check last name with les than 5 characters")
    void shouldReturnStatusBadRequestWithLastName() {
        user.setFirstName("Test1");
        user.setLastName("b");
        ResponseEntity response = userService.checkUser(user);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }


}

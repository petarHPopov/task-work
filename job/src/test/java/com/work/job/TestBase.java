package com.work.job;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;

import com.work.job.model.User;
import com.work.job.service.UserService;

public class TestBase {

    @Autowired
    protected UserService userService;

    protected static User user;



    @BeforeAll
    static void createUser() {
        user = User.builder()
                .email("test.email@email.email")
                .firstName("Test1")
                .lastName("Lastname")
                .build();
    }
}
